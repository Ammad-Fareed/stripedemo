Rails.application.routes.draw do
  root :to => 'subscriptions#index'
  resources :subscriptions
  resources :sbubscriptions
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
