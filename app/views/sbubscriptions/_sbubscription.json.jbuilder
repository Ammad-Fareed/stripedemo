json.extract! sbubscription, :id, :name, :created_at, :updated_at
json.url sbubscription_url(sbubscription, format: :json)
