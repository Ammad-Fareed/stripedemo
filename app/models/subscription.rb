class Subscription < ApplicationRecord

    def save_with_payment(token)
        if valid?
            customer = Stripe::Customer.create({
            source: token,
            email: "ammad.fareed@teknuk.com"
            })
            charge = Stripe::Charge.create({
            amount: 500,
            currency: "usd",
            customer: customer.id,
            description: "My First Test Charge (created for API docs)"
            })
            save!
        end
    end
end
