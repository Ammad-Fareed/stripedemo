class SbubscriptionsController < ApplicationController
  before_action :set_sbubscription, only: [:show, :edit, :update, :destroy]

  # GET /sbubscriptions
  # GET /sbubscriptions.json
  def index
    @sbubscriptions = Sbubscription.all
  end

  # GET /sbubscriptions/1
  # GET /sbubscriptions/1.json
  def show
  end

  # GET /sbubscriptions/new
  def new
    @sbubscription = Sbubscription.new
  end

  # GET /sbubscriptions/1/edit
  def edit
  end

  # POST /sbubscriptions
  # POST /sbubscriptions.json
  def create
    @sbubscription = Sbubscription.new(sbubscription_params)

    respond_to do |format|
      if @sbubscription.save
        format.html { redirect_to @sbubscription, notice: 'Sbubscription was successfully created.' }
        format.json { render :show, status: :created, location: @sbubscription }
      else
        format.html { render :new }
        format.json { render json: @sbubscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sbubscriptions/1
  # PATCH/PUT /sbubscriptions/1.json
  def update
    respond_to do |format|
      if @sbubscription.update(sbubscription_params)
        format.html { redirect_to @sbubscription, notice: 'Sbubscription was successfully updated.' }
        format.json { render :show, status: :ok, location: @sbubscription }
      else
        format.html { render :edit }
        format.json { render json: @sbubscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sbubscriptions/1
  # DELETE /sbubscriptions/1.json
  def destroy
    @sbubscription.destroy
    respond_to do |format|
      format.html { redirect_to sbubscriptions_url, notice: 'Sbubscription was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sbubscription
      @sbubscription = Sbubscription.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def sbubscription_params
      params.require(:sbubscription).permit(:name)
    end
end
