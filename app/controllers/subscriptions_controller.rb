class SubscriptionsController < ApplicationController
  before_action :set_subscription, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token


  # GET /subscriptions
  # GET /subscriptions.json
  def index
    @subscriptions = Subscription.all
  end

  # GET /subscriptions/1
  # GET /subscriptions/1.json
  def show
  end

  # GET /subscriptions/new
  def new
    @subscription = Subscription.new
  end

  # GET /subscriptions/1/edit
  def edit
  end
  
  # POST /subscriptions
  # POST /subscriptions.json
  def create
    @subscription = Subscription.new(subscription_params)
    token= Stripe::Token.create({
      card: {
        number: params[:card_number],
        exp_month: params[:card_month],
        exp_year: params[:card_year],
        cvc: params[:card_code],
      },
    })
    if @subscription.save_with_payment(token)
      redirect_to @subscription, :notice => "Thank you for subscribing!"
    else
      render :new
    end
  rescue Stripe::InvalidRequestError => e
    logger.error "Invalid Request Error: #{e.message}"
    redirect_to @subscription, :notice => "Invalid Request, TRY AGAIN!"
    false
  rescue Stripe::CardError => e
    logger.error "Card Error: #{e.message}"
    redirect_to @subscription, :notice => "Card Error, TRY AGAIN!"
    false
  end

  # PATCH/PUT /subscriptions/1
  # PATCH/PUT /subscriptions/1.json
  def update
    respond_to do |format|
      if @subscription.update(subscription_params)
        format.html { redirect_to @subscription, notice: 'Subscription was successfully updated.' }
        format.json { render :show, status: :ok, location: @subscription }
      else
        format.html { render :edit }
        format.json { render json: @subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subscriptions/1
  # DELETE /subscriptions/1.json
  def destroy
    @subscription.destroy
    respond_to do |format|
      format.html { redirect_to subscriptions_url, notice: 'Subscription was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subscription
      @subscription = Subscription.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def subscription_params
      params.require(:subscription).permit(:name, :email, :card_number, :card_code, :card_month, :card_year)
    end
end
