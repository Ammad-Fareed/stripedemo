# README

Please visit 'https://sleepy-sands-72771.herokuapp.com/subscriptions/new' and press the subscribe button. If the payment is successful, the page will be redirected with success message. Another page will be reloaded in case of any error with error details.

Steps for setup:
1- Clone the repo
2- run 'bundle install'
3- run 'rails s'
4- Enter email in textbox
5- Valid Card Number is '4242424242424242'
6- Enter CVV number
7- Choose valid month and year from dropdown
8- Hit enter, if the information is given valid, the you'll be subscribed. If not then try again carefully...!!!
9- Peace out...........