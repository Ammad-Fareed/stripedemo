require 'test_helper'

class SbubscriptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sbubscription = sbubscriptions(:one)
  end

  test "should get index" do
    get sbubscriptions_url
    assert_response :success
  end

  test "should get new" do
    get new_sbubscription_url
    assert_response :success
  end

  test "should create sbubscription" do
    assert_difference('Sbubscription.count') do
      post sbubscriptions_url, params: { sbubscription: { name: @sbubscription.name } }
    end

    assert_redirected_to sbubscription_url(Sbubscription.last)
  end

  test "should show sbubscription" do
    get sbubscription_url(@sbubscription)
    assert_response :success
  end

  test "should get edit" do
    get edit_sbubscription_url(@sbubscription)
    assert_response :success
  end

  test "should update sbubscription" do
    patch sbubscription_url(@sbubscription), params: { sbubscription: { name: @sbubscription.name } }
    assert_redirected_to sbubscription_url(@sbubscription)
  end

  test "should destroy sbubscription" do
    assert_difference('Sbubscription.count', -1) do
      delete sbubscription_url(@sbubscription)
    end

    assert_redirected_to sbubscriptions_url
  end
end
