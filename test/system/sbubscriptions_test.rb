require "application_system_test_case"

class SbubscriptionsTest < ApplicationSystemTestCase
  setup do
    @sbubscription = sbubscriptions(:one)
  end

  test "visiting the index" do
    visit sbubscriptions_url
    assert_selector "h1", text: "Sbubscriptions"
  end

  test "creating a Sbubscription" do
    visit sbubscriptions_url
    click_on "New Sbubscription"

    fill_in "Name", with: @sbubscription.name
    click_on "Create Sbubscription"

    assert_text "Sbubscription was successfully created"
    click_on "Back"
  end

  test "updating a Sbubscription" do
    visit sbubscriptions_url
    click_on "Edit", match: :first

    fill_in "Name", with: @sbubscription.name
    click_on "Update Sbubscription"

    assert_text "Sbubscription was successfully updated"
    click_on "Back"
  end

  test "destroying a Sbubscription" do
    visit sbubscriptions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Sbubscription was successfully destroyed"
  end
end
